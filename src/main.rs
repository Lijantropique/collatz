use std::io::{self, Write};
fn main() {
    collatz::banner("🔢 Collatz Sequence, or, the 3n + 1 Problem 🔢");
    collatz::instructions();
    let n:i32 = collatz::get_number("Enter a starting number (greater than 0) or QUIT:");
    collatz::print_sequence(n);
    io::stdout().flush().unwrap();
}
