use std::io::{self, Write};

pub fn banner(msg: &str) {
    println!(
        "----------------------
{msg}
By Al Sweigart al@inventwithpython.com
(rust-ed 🦀 by lij)
----------------------\n"
    );
}

pub fn instructions(){
    println!("The Collatz sequence is a sequence of numbers produced from a starting number n, following three rules:
\t* If n is even, the next number n is n / 2
\t* If n is odd, the next number n is n * 3 + 1
\t* If n is 1, stop\n")
}

pub fn get_input(msg: &str) -> String {
    println!("{msg}");
    print!("> ");
    let mut raw = String::new();
    io::stdout().flush().unwrap();
    io::stdin()
        .read_line(&mut raw)
        .expect("Failed to read line");
    raw.trim().to_string()
}

pub fn get_number(msg: &str) -> i32 {
    let mut n : i32;
    loop {
        let raw = get_input(msg);
        n = match raw.parse(){
            Ok(n) => n,
            Err(_) => {
                println!("Please enter a number greater than zero");
                -1
            },
        };
        if n>0 {break;}
    }
    n
}

pub fn print_sequence(n:i32){
    print!("{} ", n);
    match n {
        1 => print!("1\n"),
       i if i %2 == 0 => print_sequence(n/2),
       _ => print_sequence(n*3+1),
    }
}